import javax.swing.*;
import java.awt.event.*;
class Calc implements ActionListener
{
	JFrame f;
	JTextField t;
	JButton[] x=new JButton[10];
	JButton bdiv,bmul,bsub,badd,bdec,beq,bdel,bclr;
	static double a=0,b=0,result=0,c;
	static int i=0, j=0,k=0,l=0,m=0,n=0,operator;
	
	Calc()
	{
		f=new JFrame("Caluculator");
		t=new JTextField();
		for(int i=0;i<10;i++)
		{
			x[i]=new JButton(""+i);
		}
		bdiv=new JButton("/");
		bmul=new JButton("*");
		bsub=new JButton("-");
		badd=new JButton("+");
		bdec=new JButton(".");
		beq=new JButton("=");
		bdel=new JButton("Delete");
		bclr=new JButton("Clear");
		
			
		t.setBounds(30,40,280,30);
		x[7].setBounds(40,100,50,40);
		x[8].setBounds(110,100,50,40);
		x[9].setBounds(180,100,50,40);
		bdiv.setBounds(250,100,50,40);

		x[4].setBounds(40,170,50,40);
		x[5].setBounds(110,170,50,40);
		x[6].setBounds(180,170,50,40);
		bmul.setBounds(250,170,50,40);
			
		x[1].setBounds(40,240,50,40);
		x[2].setBounds(110,240,50,40);
		x[3].setBounds(180,240,50,40);
		bsub.setBounds(250,240,50,40);
		
		bdec.setBounds(40,310,50,40);
		x[0].setBounds(110,310,50,40);
		beq.setBounds(180,310,50,40);
		badd.setBounds(250,310,50,40);
		
		bdel.setBounds(60,380,100,40);
		bclr.setBounds(180,380,100,40);
		
		for(int i=0;i<10;i++)
		{
			f.add(x[i]);
		}
		f.add(t);
		f.add(bdiv);
		f.add(bmul);
		f.add(bsub);
		f.add(bdec);
		f.add(beq);
		f.add(badd);
		f.add(bdel);
		f.add(bclr);
		
		f.setLayout(null);
		f.setVisible(true);
		f.setSize(350,500);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setResizable(false);
		
		x[1].addActionListener(this);
		x[2].addActionListener(this);
		x[3].addActionListener(this);
		x[4].addActionListener(this);
		x[5].addActionListener(this);
		x[6].addActionListener(this);
		x[7].addActionListener(this);
		x[8].addActionListener(this);
		x[9].addActionListener(this);
		x[0].addActionListener(this);
		badd.addActionListener(this);
		bdiv.addActionListener(this);
		bmul.addActionListener(this);
		bsub.addActionListener(this);
		bdec.addActionListener(this);
		beq.addActionListener(this);
		bdel.addActionListener(this);
		bclr.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==x[1])
			t.setText(t.getText().concat("1"));
		if(e.getSource()==x[2])
			t.setText(t.getText().concat("2"));
		if(e.getSource()==x[3])
			t.setText(t.getText().concat("3"));
		if(e.getSource()==x[4])
			t.setText(t.getText().concat("4"));
		if(e.getSource()==x[5])
			t.setText(t.getText().concat("5"));
		if(e.getSource()==x[6])
			t.setText(t.getText().concat("6"));
		if(e.getSource()==x[7])
			t.setText(t.getText().concat("7"));
		if(e.getSource()==x[8])
			t.setText(t.getText().concat("8"));
		if(e.getSource()==x[9])
			t.setText(t.getText().concat("9"));
		if(e.getSource()==x[0])
			t.setText(t.getText().concat("0"));
		if(e.getSource()==bdec)
			t.setText(t.getText().concat("."));
		
		
		if(e.getSource()==badd)
		{	operator=1;
			if(j==0)
			{
			a=Double.parseDouble(t.getText());
			j++;
			t.setText("");
			if(result==0)
				{
				result=a;
				}
			}
			else
			{
			b=Double.parseDouble(t.getText());
			result=result+b;
			t.setText("");
			}
		}
		if(e.getSource()==bsub)
		{	
			operator=2;
			if(j==0)
			{
			a=Double.parseDouble(t.getText());
			j++;
			t.setText("");
			if(result==0)
				{
				result=a;
				}
			}
			else
			{
			b=Double.parseDouble(t.getText());
			result=result-b;
			t.setText("");
			}
		}
		if(e.getSource()==bmul)
		{
			operator=3;
			if(j==0)
			{
			a=Double.parseDouble(t.getText());
			j++;
			t.setText("");
			if(result==0)
				{
				result=a;
				}
			}
			else
			{
			b=Double.parseDouble(t.getText());
			result=result*b;
			t.setText("");
			}
		}
		if(e.getSource()==bdiv)
		{		
			operator=4;
			if(j==0)
			{
			a=Double.parseDouble(t.getText());
			j++;
			t.setText("");
			if(result==0)
				{
				result=a;
				}
			}
			else
			{
			b=Double.parseDouble(t.getText());
			result=result/b;
			t.setText("");
			}
		}	
		if(e.getSource()==beq)
		{
			c=Double.parseDouble(t.getText());
				
			switch(operator)
			{
				case 1:result=result+c;
					break;
				case 2:result=result-c;
					break;
				case 3:result=result*c;
					break;
				case 4:result=result/c;
					break;
				default:result=result;
			}
		
			t.setText("");
			t.setText(""+result);
		}
		if(e.getSource()==bclr){
			t.setText("");
			j=0;
		}
		if(e.getSource()==bdel)
		{
			String s=t.getText();
			t.setText("");
			for(int i=0;i<s.length()-1;i++)
			t.setText(t.getText()+s.charAt(i));
		}
  }

  public static void main(Strings args[])
	{

		new Calc();
	}
}


	

		
		